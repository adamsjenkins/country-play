import { Euler } from "react-three-fiber";

import { getRandomColor } from "../libs/utils";
export { default as FLAGS_BY_NAME } from "../data/flags/flags-from-names.json";
export { default as FLAG_PNGS_BY_NAME } from "../data/flags/flags-from-names-with-require";

// export const GLOBE_COUNTRY_SCALE = 50;

/**
 * globe_lo2.geo.canMod.json
 * has extra point for canada and United states, so the line doesn't go
 * through the world at the long border between them:
 *
 *             [-95.15, 49],
 * -> added -> [-108, 49.84],
 *             [-122.76, 49],
 */
// import geoJsonCountries from "../data/globe_lo2.geo.canMod2.json";
// From: https://raw.githubusercontent.com/protectwise/troika/master/packages/troika-examples/globe-connections/countries.geojson.json
import geoJsonCountries from "../data/countries.geojson.mod.json";
import { sphericalCoordinates } from "../types";
// import { Euler } from "three";

/**
 * Sizes
 */
export const GLOBE_COUNTRY_SCALE = 30;
export const GLOBE__COORD_TO_SPHERE__SPHERE_RADIUS = 30;
/** The radius for the full earth with image overlay over sphere */
export const PLANET_RADIUS_SPHERE = GLOBE_COUNTRY_SCALE * 0.995;

/** Position of where the initial Camera orbit will be */
export const CAMERA_ORBIT_RADIUS = 250;
/** How far out you can zoom */
export const MAX_CAMERA_ORBIT_RADIUS = 250;
/** How far in you can zoom */
export const MIN_CAMERA_ORBIT_RADIUS = 25;
export const INITIAL_CAMERA_POSITION: Euler = [0, 0, CAMERA_ORBIT_RADIUS];
export const INITIAL_CAMERA_SPHERICAL_COORDS: sphericalCoordinates = [
  CAMERA_ORBIT_RADIUS,
  Math.PI / 2,
  0,
];

/**
 * All Countries data
 * geojson and flags and anything else
 */
export const COUNTRIES_DATA = geoJsonCountries;
// ID of "country" being selected when it's actually the globe being selected.
export const COUNTRY_ID_GLOBE = -1;

// The features also includes properties. e.g.:
//  "properties": Object {
//   "abbrev": "Afg.",
//   "continent": "Asia",
//   "formal_en": "Islamic State of Afghanistan",
//   "iso_a2": "AF",
//   "iso_a3": "AFG",
//   "name": "Afghanistan",
//   "name_long": "Afghanistan",
//   "region_un": "Asia",
//   "region_wb": "South Asia",
//   "subregion": "Southern Asia",
// },
export const COUNTRIES_DATA_FEATURES = COUNTRIES_DATA?.features;
export const NUM_COUNTRIES = COUNTRIES_DATA_FEATURES.length;

// "subregion": "Southern Asia",
// "subregion": "Australia and New Zealand",
// "subregion": "South-Eastern Asia",
// "subregion": "Eastern Asia",
// "subregion": "Melanesia",
// "subregion": "Middle Africa",
// "subregion": "Southern Europe",
// "subregion": "Western Asia",
// "subregion": "Seven seas (open ocean)",
// "subregion": "Western Europe",
// "subregion": "Eastern Africa",
// "subregion": "Western Africa",
// "subregion": "Eastern Europe",
// "subregion": "Southern Africa",
// "subregion": "Northern Europe",
// "subregion": "Northern Africa",
// "subregion": "Northern America",
// "subregion": "Central Asia",
// "subregion": "South America",
// "subregion": "Caribbean",
// "subregion": "Central America",
// "subregion": "Antarctica",
export let COUNTRIES_SUBREGIONS = [];

// "region_un": "Asia",
// "region_un": "Oceania",
// "region_un": "Africa",
// "region_un": "Europe",
// "region_un": "Seven seas (open ocean)",
// "region_un": "Americas",
// "region_un": "Antarctica",

// "continent": "Asia",
// "continent": "Oceania",
// "continent": "Africa",
// "continent": "Europe",
// "continent": "Seven seas (open ocean)",
// "continent": "North America",
// "continent": "South America",
// "continent": "Antarctica",
export let COUNTRIES_CONTINENTS = [];
export let COUNTRY_IDS_FOR_SUBREGIONS = {};
export let COUNTRY_IDS_FOR_CONTINENTS = {};
export let COUNTRIES_SUBREGIONS_COLORS = {};
export let COUNTRIES_CONTINENTS_COLORS = {};

/** Populate the continents and subregions and their colors. */
COUNTRIES_DATA_FEATURES.forEach((feature, countryId) => {
  const { subregion, continent } = feature?.properties;

  if (subregion && COUNTRIES_SUBREGIONS.indexOf(subregion) < 0) {
    COUNTRIES_SUBREGIONS.push(subregion);
    COUNTRIES_SUBREGIONS_COLORS[subregion] = getRandomColor();
  }

  if (subregion && !COUNTRY_IDS_FOR_SUBREGIONS[subregion]) {
    COUNTRY_IDS_FOR_SUBREGIONS[subregion] = [];
  }
  COUNTRY_IDS_FOR_SUBREGIONS[subregion].push(countryId);

  if (continent && COUNTRIES_CONTINENTS.indexOf(continent) < 0) {
    COUNTRIES_CONTINENTS.push(continent);
    COUNTRIES_CONTINENTS_COLORS[continent] = getRandomColor();
  }

  if (continent && !COUNTRY_IDS_FOR_CONTINENTS[continent]) {
    COUNTRY_IDS_FOR_CONTINENTS[continent] = [];
  }
  COUNTRY_IDS_FOR_CONTINENTS[continent].push(countryId);
});

/*
 Step one.
 1) Message:
    Select a sub-region to practice
 2) Select Subregion...
    make subregion in atom store
    mark game mode to country_in_subregion

  3)disable all countries except the subregion.
  
 4) getNextCountryToSelect
    - save that to atom for subregionToSelect.
 5) If user selects correct
    - Mark as correct in atom store
      - In some kinda store.
    - 
  6) Then check if all done.
  7) otherwise getNextCountryToSelect (step 4)
  8) When all done show results
  9) Let them "try again" or "return to subregion select"

*/