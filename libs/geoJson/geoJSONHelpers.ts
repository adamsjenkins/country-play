import { Vector2, Vector3, Geometry, Face3, Sphere } from "three";
import {
  COUNTRIES_DATA_FEATURES,
  FLAGS_BY_NAME,
  GLOBE__COORD_TO_SPHERE__SPHERE_RADIUS,
  NUM_COUNTRIES,
} from "../../constants";
import {
  geoJSONGeometryCoordinates,
  geoJSONLinearRing,
  GeometryType,
} from "../../types";

/** Detect if the id number is valid */
export const isValidCountryId = (countryId: number) =>
  !(0 <= countryId && countryId < NUM_COUNTRIES);

/**
 * Get the center of a group of coordinates.
 * This is mostly used to find the center of a country
 * so the name can be placed at its center
 *
 * Later we will hardcord these points so they don't need
 * to be calculated at runtime.
 *
 * And so specific exceptions can be made.
 */
export const getCenterCoordinateForCountryId = (countryId) => {
  
  const coords = COUNTRIES_DATA_FEATURES[countryId]?.geometry?.coordinates;
  const geometryType = getGeometryTypeFromCountryId(countryId);


  if (geometryType === GeometryType.MultiPolygon) {
    return getCenterCoordinateForMultiPolygonCoordinate(coords as number[][][][]);
  }

  if (geometryType === GeometryType.Polygon) {
    return getCenterCoordinateForPolygonCoordinate(coords as number[][][]);
  }

  return null;
};

export const getCenterCoordinateForMultiPolygonCoordinate = (
  coords: number[][][][]
) => {
  
  // For now just return the center point for the first polygon
  return getCenterCoordinateForPolygonCoordinate(coords[0]);
};

export const getCenterCoordinateForPolygonCoordinate = (
  coords: number[][][]
) => {
  // Polygon coordinate will have structure
  // (see: https://tools.ietf.org/html/rfc7946#appendix-A.3)
  //
  // [
  //   [
  //     [
  //       -97.14,
  //       25.97,
  //     ],
  //    ...
  //     [
  //       -97.14,
  //       25.97,
  //     ],
  //   ]
  // ]
  // This is the structure of a coordinate
  // See: https://tools.ietf.org/html/rfc7946#appendix-A.3
  // Note: generally there won't be any interiorRingCoords
  const [exteriorRingCoords, interiorRingCoords] = coords;

  
  if (!exteriorRingCoords) return;

  // console.log("c_ jsut returning first point");
  // return exteriorRingCoords[0];

  const numCoords = exteriorRingCoords?.length;

  // Sum up all coordinate positions;
  const coordsSum = exteriorRingCoords?.reduce((prevVal, curVal) => [
    prevVal[0] + curVal[0],
    prevVal[1] + curVal[1],
  ]);

  const coordsAvg = [coordsSum[0] / numCoords, coordsSum[1] / numCoords];

  return coordsAvg;
};

export const getCenterSphereCoordinateForCountryId = ({
  countryId,
  sphereRadius = GLOBE__COORD_TO_SPHERE__SPHERE_RADIUS
}: {
  countryId: number;
  sphereRadius?: number;
}) => {
  
  const centerCoord = getCenterCoordinateForCountryId(countryId);
  
  if (!centerCoord || centerCoord.length !== 2) return null;

  return convertCoordToSphereCoord({
    coordinate: centerCoord as [longitude: number, latitude: number],
    sphereRadius,
  });
};

// ============================================

/** Returns geometry type for country by its id
 * e.g. MultiPolygon (which has coordinates number[][][][])
 * or Polygon (which has coordinates number[][][])
 */
export const getGeometryTypeFromCountryId = (countryId: number) =>
  COUNTRIES_DATA_FEATURES[countryId]?.geometry?.type;

/** Returns coordiantes from country feature */
export const getCoordsFromFeature = (feature) => {
  const { geometry } = feature;
  const { type: geometryType, coordinates } = geometry;

  const isMultiplePolygons = geometryType === "MultiPolygon";

  // next things to do..
  // make this (or some function) return
  // all the sphere coordinates for a feature
  // (isMultiplePolygons and regular polygon
  //
  // Then use that to draw lines
  //
  // Then also onto the bit about making triangles to fill it...
  // ... however the fuck that works.
  //
  if (isMultiplePolygons) {
    console.log("more work here....");
    return "VALUE HERE";
  }

  console.log("more work here....");
  return "VALUE HERE";
};

export function getSphereCoordsFromFeature({ geoJSONFeature }) {
  const coords = getCoordsFromFeature(geoJSONFeature);
}

export function convertGeoJSONGeometryCoordsTo3dSphereCoords({
  geometryType,
  geometryCoordinates,
  sphereRadius = GLOBE__COORD_TO_SPHERE__SPHERE_RADIUS
}: {
  // coordinatesArray: [longitude: number, latitude: number][];
  geometryType: GeometryType | string;
  geometryCoordinates: geoJSONGeometryCoordinates; //[longitude: number, latitude: number][];
  sphereRadius?: number;
}) {
  if (
    geometryType === GeometryType.MultiPolygon &&
    geometryCoordinates?.length > 0
  ) {
    return geometryCoordinates.map((polyGeometryCoordinates) =>
      convertGeoJSONGeometryCoordsTo3dSphereCoords({
        geometryType: GeometryType.Polygon,
        geometryCoordinates: polyGeometryCoordinates,
        sphereRadius,
      })
    );
  }

  if (geometryType === GeometryType.Polygon) {
    const exteriorRingCoords = geometryCoordinates?.[0];

    return exteriorRingCoords?.map((coord) =>
      convertCoordToSphereCoord({
        coordinate: coord,
        sphereRadius,
      })
    );
  }

  return [];
}

export function convertCoordToSphereCoord({
  coordinate,
  sphereRadius = GLOBE__COORD_TO_SPHERE__SPHERE_RADIUS
}: {
  coordinate: [longitude: number, latitude: number];
  sphereRadius?: number;
}) {
  if (!coordinate || coordinate.length !== 2) {
    return new Vector3(0,0,0);
  }

  var lon = coordinate?.[0];
  var lat = coordinate?.[1];

  const x =
    Math.cos((lat * Math.PI) / 180) *
    Math.cos((lon * Math.PI) / 180) *
    sphereRadius;
  const y = Math.sin((lat * Math.PI) / 180) * sphereRadius;
  const z =
    Math.cos((lat * Math.PI) / 180) *
    Math.sin((lon * Math.PI) / 180) *
    sphereRadius * -1;
  // The original way:
  // const y =
  //   Math.cos((lat * Math.PI) / 180) *
  //   Math.sin((lon * Math.PI) / 180) *
  //   sphereRadius;
  // const z = Math.sin((lat * Math.PI) / 180) * sphereRadius;

  return new Vector3(x, y, z);
  // return { x, y, z };
}

/** 
 * This originally worked, but seems off currently.
 * Perhaps the way I'm drawing things now is a bit different.
 */
export function convertCoordToSphereCoordOld({
  coordinate,
  sphereRadius = GLOBE__COORD_TO_SPHERE__SPHERE_RADIUS
}: {
  coordinate: [longitude: number, latitude: number];
  sphereRadius?: number;
}) {
  if (!coordinate || coordinate.length !== 2) {
    return new Vector3(0,0,0);
  }

  var lon = coordinate?.[0];
  var lat = coordinate?.[1];

  const x =
    Math.cos((lat * Math.PI) / 180) *
    Math.cos((lon * Math.PI) / 180) *
    sphereRadius;
  const y =
    Math.cos((lat * Math.PI) / 180) *
    Math.sin((lon * Math.PI) / 180) *
    sphereRadius;
  const z = Math.sin((lat * Math.PI) / 180) * sphereRadius;

  return new Vector3(x, y, z);
  // return { x, y, z };
}

// from : https://bl.ocks.org/marcopompili/f5e071ce646c5cf3d600828ace734ce7
function needsInterpolation(point2: number[], point1: number[]) {
  //If the distance between two latitude and longitude values is
  //greater than five degrees, return true.
  var lon1 = point1[0];
  var lat1 = point1[1];
  var lon2 = point2[0];
  var lat2 = point2[1];
  var lon_distance = Math.abs(lon1 - lon2);
  var lat_distance = Math.abs(lat1 - lat2);

  if (lon_distance > 5 || lat_distance > 5) {
    return true;
  } else {
    return false;
  }
}

function getMidpoint(point1: number[], point2: number[]) {
  var midpoint_lon = (point1[0] + point2[0]) / 2;
  var midpoint_lat = (point1[1] + point2[1]) / 2;
  var midpoint = [midpoint_lon, midpoint_lat];

  return midpoint;
}

function interpolatePoints(interpolation_array) {
  //This function is recursive. It will continue to add midpoints to the
  //interpolation array until needsInterpolation() returns false.
  var temp_array = [];
  var point1, point2;

  for (
    var point_num = 0;
    point_num < interpolation_array.length - 1;
    point_num++
  ) {
    point1 = interpolation_array[point_num];
    point2 = interpolation_array[point_num + 1];

    if (needsInterpolation(point2, point1)) {
      temp_array.push(point1);
      temp_array.push(getMidpoint(point1, point2));
    } else {
      temp_array.push(point1);
    }
  }

  temp_array.push(interpolation_array[interpolation_array.length - 1]);

  if (temp_array.length > interpolation_array.length) {
    temp_array = interpolatePoints(temp_array);
  } else {
    return temp_array;
  }
  return temp_array;
}

// Loop through the coordinates and figure out if the points need interpolation.
function createCoordinateArray(feature) {
  var temp_array = [];
  var interpolation_array = [];

  for (var point_num = 0; point_num < feature.length; point_num++) {
    var point1 = feature[point_num];
    var point2 = feature[point_num - 1];

    if (point_num > 0) {
      if (needsInterpolation(point2, point1)) {
        interpolation_array = [point2, point1];
        interpolation_array = interpolatePoints(interpolation_array);

        for (
          var inter_point_num = 0;
          inter_point_num < interpolation_array.length;
          inter_point_num++
        ) {
          temp_array.push(interpolation_array[inter_point_num]);
        }
      } else {
        temp_array.push(point1);
      }
    } else {
      temp_array.push(point1);
    }
  }
  return temp_array;
}

// from:
//            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
//                    Version 2, December 2004
//
// Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
//
// Everyone is permitted to copy and distribute verbatim or modified
// copies of this license document, and changing it is allowed as long
// as the name is changed.
//
//            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
//   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
//
//  0. You just DO WHAT THE FUCK YOU WANT TO.

// From: https://makc3d.wordpress.com/2014/04/20/threejs-bar-chart-on-the-globe/
// and http://makc.github.io/three.js/map2globe/
// http://makc.github.io/three.js/map2globe/demo.html
export function Map3DGeometry(data, innerRadius) {
  if (
    arguments.length < 2 ||
    isNaN(parseFloat(innerRadius)) ||
    !isFinite(innerRadius) ||
    innerRadius < 0
  ) {
    // if no valid inner radius is given, do not extrude
    innerRadius = 42;
  }

  Geometry.call(this);
  // data.vertices = [lat, lon, ...]
  // data.polygons = [[poly indices, hole i-s, ...], ...]
  // data.triangles = [tri i-s, ...]
  var i,
    uvs = [];
  for (i = 0; i < data.vertices.length; i += 2) {
    var lon = data.vertices[i];
    var lat = data.vertices[i + 1];
    // colatitude
    var phi = +(90 - lat) * 0.01745329252;
    // azimuthal angle
    var the = +(180 - lon) * 0.01745329252;
    // translate into XYZ coordinates
    var wx = Math.sin(the) * Math.sin(phi) * -1;
    var wz = Math.cos(the) * Math.sin(phi);
    var wy = Math.cos(phi);
    // equirectangular projection
    var wu = 0.25 + lon / 360.0;
    var wv = 0.5 + lat / 180.0;

    this.vertices.push(new Vector3(wx, wy, wz));

    uvs.push(new Vector2(wu, wv));
  }

  var n = this.vertices.length;

  if (innerRadius <= 1) {
    for (i = 0; i < n; i++) {
      var v = this.vertices[i];
      this.vertices.push(v.clone().multiplyScalar(innerRadius));
    }
  }

  for (i = 0; i < data.triangles.length; i += 3) {
    var a = data.triangles[i];
    var b = data.triangles[i + 1];
    var c = data.triangles[i + 2];

    this.faces.push(
      new Face3(a, b, c, [this.vertices[a], this.vertices[b], this.vertices[c]])
    );
    this.faceVertexUvs[0].push([uvs[a], uvs[b], uvs[c]]);

    if (0 < innerRadius && innerRadius <= 1) {
      this.faces.push(
        new Face3(n + b, n + a, n + c, [
          this.vertices[b].clone().multiplyScalar(-1),
          this.vertices[a].clone().multiplyScalar(-1),
          this.vertices[c].clone().multiplyScalar(-1),
        ])
      );
      this.faceVertexUvs[0].push([uvs[b], uvs[a], uvs[c]]); // shitty uvs to make 3js exporter happy
    }
  }

  // extrude
  if (innerRadius < 1) {
    for (i = 0; i < data.polygons.length; i++) {
      var polyWithHoles = data.polygons[i];
      for (var j = 0; j < polyWithHoles.length; j++) {
        var polygonOrHole = polyWithHoles[j];
        for (var k = 0; k < polygonOrHole.length; k++) {
          var a = polygonOrHole[k],
            b = polygonOrHole[(k + 1) % polygonOrHole.length];
          var va1 = this.vertices[a],
            vb1 = this.vertices[b];
          var va2 = this.vertices[n + a],
            vb2 = this.vertices[n + b];
          var normal;
          if (j < 1) {
            // polygon
            normal = vb1
              .clone()
              .sub(va1)
              .cross(va2.clone().sub(va1))
              .normalize();
            this.faces.push(new Face3(a, b, n + a, [normal, normal, normal]));
            this.faceVertexUvs[0].push([uvs[a], uvs[b], uvs[a]]); // shitty uvs to make 3js exporter happy
            if (innerRadius > 0) {
              this.faces.push(
                new Face3(b, n + b, n + a, [normal, normal, normal])
              );
              this.faceVertexUvs[0].push([uvs[b], uvs[b], uvs[a]]); // shitty uvs to make 3js exporter happy
            }
          } else {
            // hole
            normal = va2
              .clone()
              .sub(va1)
              .cross(vb1.clone().sub(va1))
              .normalize();
            this.faces.push(new Face3(b, a, n + a, [normal, normal, normal]));
            this.faceVertexUvs[0].push([uvs[b], uvs[a], uvs[a]]); // shitty uvs to make 3js exporter happy
            if (innerRadius > 0) {
              this.faces.push(
                new Face3(b, n + a, n + b, [normal, normal, normal])
              );
              this.faceVertexUvs[0].push([uvs[b], uvs[a], uvs[b]]); // shitty uvs to make 3js exporter happy
            }
          }
        }
      }
    }
  }

  this.computeFaceNormals();

  this.boundingSphere = new Sphere(new Vector3(), 1);
}

Map3DGeometry.prototype = Object.create(Geometry.prototype);
// export { Map3DGeometry };

/**
 * Get properties from Country Id
 */
export const getCountryNameFromId = (countryId: number) =>
  COUNTRIES_DATA_FEATURES?.[countryId]?.properties?.name;
export const getCountrySubregionFromId = (countryId: number) =>
  COUNTRIES_DATA_FEATURES?.[countryId]?.properties?.subregion;
export const getCountryContinentFromId = (countryId: number) =>
  COUNTRIES_DATA_FEATURES?.[countryId]?.properties?.continent;
export const getCountryRegionUNFromId = (countryId: number) =>
  COUNTRIES_DATA_FEATURES?.[countryId]?.properties?.region_un;
export const getCountryCoordinatesFromId = (countryId: number) =>
  COUNTRIES_DATA_FEATURES?.[countryId]?.geometry?.coordinates;
export const getCountryFlagCodeFromId = (countryId: number) => {
  const name = getCountryNameFromId(countryId);
  const flagCode = FLAGS_BY_NAME?.[name];
  return flagCode;
};
