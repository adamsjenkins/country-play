import * as THREE from "three";
import React, { useRef, useState } from "react";
import { Canvas, useFrame, useUpdate } from "react-three-fiber";

function BoxWithEdges(props) {
  const { size } = props;
  const mesh = useRef();
  const seg = useUpdate((s) => {
    s.geometry = new THREE.EdgesGeometry(mesh.current.geometry);
    // const edgeGeo = new THREE.EdgesGeometry(mesh.current.geometry);
    // const edgeMaterial = new THREE.LineBasicMaterial( { color: 0xffff00, linewidth: 2 } )
    // const edges = new THREE.LineSegments( edgeGeo, edgeMaterial );

  }, []);
  const [hovered, setHover] = useState(false);
  const [active, setActive] = useState(false);
  useFrame(() => (mesh.current.rotation.x = mesh.current.rotation.y += 0.01));
  return (
    <group scale={[size, size, size]}>
      <mesh
        ref={mesh}
        {...props}
        scale={active ? [1.5, 1.5, 1.5] : [1, 1, 1]}
        onClick={(e) => setActive(!active)}
        onPointerOver={(e) => setHover(true)}
        onPointerOut={(e) => setHover(false)}
      >
        <boxBufferGeometry args={[1, 1, 1]} />
        <meshStandardMaterial color={hovered ? "hotpink" : "orange"} />
        <lineSegments ref={seg}>
          {/* <meshBasicMaterial color="black" wireframeLinewidth={20} lineWidth={30}/> */}
          <lineBasicMaterial color={0x000000} linewidth={10}/>
          {/* <lineBasicMaterial color={0xffff00} linewidth={20}/> */}
        </lineSegments>
      </mesh>
    </group>
  );
}

export default BoxWithEdges;
