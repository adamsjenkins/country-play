import React, { useRef } from "react";
import { TextureLoader } from "expo-three";
import { PLANET_RADIUS_SPHERE } from "../../../constants";
const earthNaturalImgSrc = require("../../../assets/images/earth/earth.png");
const earthTexture = new TextureLoader().load(earthNaturalImgSrc);

const Sphere = (props) => {
  // const gameMode = GameMode.SelectContinent; // useRecoilValue(gameModeState);
  // This reference will give us direct access to the mesh
  const mesh = useRef();
  const scale = 1;
  const meshTexture = earthTexture;

  return (
    <mesh {...props} ref={mesh} scale={[scale, scale, scale]}>
      <sphereBufferGeometry
        attach="geometry"
        args={[PLANET_RADIUS_SPHERE, 64, 64]}
      />
      <meshStandardMaterial map={meshTexture} roughness={1} fog={false} />
    </mesh>
  );
};

export default Sphere;
