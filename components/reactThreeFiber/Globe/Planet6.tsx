import React, { useRef } from "react";
import { TextureLoader } from "expo-three";
import { PLANET_RADIUS_SPHERE } from "../../../constants";

const earthNaturalImgSrc = require("../../../assets/images/earth/earth.png");
const earthNightImgSrc = require("../../../assets/images/earth/earth_night-min.png");
const earthImgSrc = require("../../../assets/images/earth/3_no_ice_clouds_8k-min.png");

// const earthTexture = new TextureLoader().load(earthNaturalImgSrc);
// const earthTexture = new TextureLoader().load(earthNightImgSrc);
const earthTexture = new TextureLoader().load(earthImgSrc);

export const Planet6 = (props) => {
  // This reference will give us direct access to the mesh
  const mesh = useRef();

  const scale = 1;

  // const handleClick = () => {
  //   console.log("clicked Planet6");
  // };

  const meshTexture = earthTexture;

  return (
    <mesh
      {...props}
      ref={mesh}
      scale={[scale, scale, scale]}
      // onClick={handleClick}
    >
      <sphereBufferGeometry
        attach="geometry"
        args={[PLANET_RADIUS_SPHERE, 64, 64]}
      />
      <meshStandardMaterial map={meshTexture} roughness={1} fog={false} />
    </mesh>
  );
};

export default Planet6;
