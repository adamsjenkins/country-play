import React from "react";

/* ---------------------------------- Data ---------------------------------- */
import { NUM_COUNTRIES } from "../../../constants";

/* ------------------------------- Components ------------------------------- */
import Country from "./Country";

/* ---------------------------------- Types --------------------------------- */
type CountriesProps = {};

/* -------------------------------- Component ------------------------------- */
export const Countries: React.FC<CountriesProps> = ({}) => {
  const countries = new Array(NUM_COUNTRIES)
    .fill(1)
    .map((v, i) => <Country key={`c${i}`} countryId={i} />);

  return <group>{countries}</group>;
};

export default Countries;
