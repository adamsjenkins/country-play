import React, { useState, useEffect, useRef } from "react";
import {
  Button,
  View,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  Image,
  TouchableOpacity,
} from "react-native";
import { useAppSelector, useAppDispatch } from "../../redux/hooks";
import { selectGameMode } from "../../redux/slices/gameDataSlice";
import { GameMode } from "../../types";
import { GameApi } from "../../api/GameApi";
import { useRecoilState } from "recoil";
import { gameModeState } from "../../recoil/gameData/atom";

// Making gameModes an array for easy reference.
let gameModes = [];
for (var mode in GameMode) {
  gameModes.push(mode);
}

type GameModeTesterProps = {};

//export const GameModeTester = () => {
export const GameModeTester: React.FC<GameModeTesterProps> = ({}) => {
  // Game Moded
  const [gameMode] = useRecoilState(gameModeState);
  // const refGameMode = useRef(gameMode);
  // refGameMode.current = gameMode;

  // const gameMode = useAppSelector((state) => state.gameData.gameMode);
  // const dispatch = useAppDispatch();

  const handleGameModeToggle = () => {
    // const index = gameModes.indexOf(gameMode);
    // const newGameMode = gameModes[(index + 1) % gameModes.length];
    // dispatch(selectGameMode(newGameMode));
    GameApi.toggleGameMode();
  };

  return (
    <View style={styles.container} onTouchStart={handleGameModeToggle}>
      <Text>Select-</Text>
      <Text>{gameMode}</Text>
      {/* <Text>{refGameMode.current}</Text> */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#F77",
    height: "100%",
    width: "100%",
  },
});

export default GameModeTester;
