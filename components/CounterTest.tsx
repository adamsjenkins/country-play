import React from "react";

import {
  Button,
  View,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  Image,
  TouchableOpacity,
} from "react-native";

// import { useSelector, useDispatch } from "react-redux";

// This takes care of types instead of using just `useSelector` or `useAppDispatch`
import { useAppSelector, useAppDispatch } from "../redux/hooks";
import { decrement, increment } from "../redux/slices/counterSlice";

export function Counter() {
  // The `state` arg is correctly typed as `RootState` already
  const count = useAppSelector((state) => state.counter.value);
  const dispatch = useAppDispatch();
  // @ts-ignore
  // const count = useSelector((state) => state.counter.value);
  // const dispatch = useDispatch();

  return (
    <View>
      <Text>{count}</Text>
      <Text onPress={() => dispatch(increment())}>Increment value</Text>
      <Text onPress={() => dispatch(decrement())}>Decrement value</Text>
    </View>
  );
}

// import React, { useState, useEffect, useRef } from 'react';
// import {
//   Button,
//   View,
//   SafeAreaView,
//   ScrollView,
//   StyleSheet,
//   Text,
//   Image,
//   TouchableOpacity,
// } from 'react-native';

// type Props = {};

// //export const  = () => {
// export const : React.FC<Props> = ({}) => {
//   return (
//     <View style={styles.container}>
//       <Text></Text>
//     </View>
//   );
// };

// const styles = StyleSheet.create({
//   container: {}
// });

// export default ;
