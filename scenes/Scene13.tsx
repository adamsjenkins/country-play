import React, { memo } from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import { extend, ReactThreeFiber } from "react-three-fiber";
import { useRecoilValue } from "recoil";
import { OutlinePass } from "three/examples/jsm/postprocessing/OutlinePass";
import { EffectComposer } from "three/examples/jsm/postprocessing/EffectComposer";
import { ShaderPass } from "three/examples/jsm/postprocessing/ShaderPass";
import { RenderPass } from "three/examples/jsm/postprocessing/RenderPass";
import { UnrealBloomPass } from "three/examples/jsm/postprocessing/UnrealBloomPass";
import { FXAAShader } from "three/examples/jsm/shaders/FXAAShader";

import { CountryStatus } from "../types";
import { countriesStatusState } from "../recoil/countries/atom";
import { getCountryNameFromId } from "../libs/geoJson/geoJSONHelpers";
import { FLAG_PNGS_BY_NAME } from "../constants";
import Planet6 from "../components/reactThreeFiber/Globe/Planet6";

/* ------------------------------- Components ------------------------------- */
import CanvasWithOrbitControls from "../components/CanvasWithOrbitControls";
import { Countries } from "../components/reactThreeFiber/Countries";

/* --------------------------------- Effects -------------------------------- */
extend({
  OutlinePass,
  EffectComposer,
  RenderPass,
  UnrealBloomPass,
  ShaderPass,
  FXAAShader,
});

declare global {
  namespace JSX {
    interface IntrinsicElements {
      // bloomEffect: ReactThreeFiber.Object3DNode<
      //   BloomEffect,
      //   typeof BloomEffect
      // >;
      effectComposer: ReactThreeFiber.Object3DNode<
        EffectComposer,
        typeof EffectComposer
      >;
      // effectPass: ReactThreeFiber.Object3DNode<EffectPass, typeof EffectPass>;
      renderPass: ReactThreeFiber.Object3DNode<RenderPass, typeof RenderPass>;
      // sSAOPass: ReactThreeFiber.Object3DNode<sSAOPass, typeof sSAOPass>;
      unrealBloomPass: ReactThreeFiber.Object3DNode<
        UnrealBloomPass,
        typeof UnrealBloomPass
      >;
      shaderPass: ReactThreeFiber.Object3DNode<ShaderPass, typeof ShaderPass>;
      outlinePass: ReactThreeFiber.Object3DNode<
        OutlinePass,
        typeof OutlinePass
      >;
      // 'fXAAShader': ReactThreeFiber.Object3DNode<FXAAShader, typeof FXAAShader>;
    }
  }
}

// =========================================================
// Effects;
// function Effects() {
//   // return null
//   const composer = useRef();
//   const { scene, gl, size, camera } = useThree();
//   useEffect(
//     () => void composer?.current?.setSize(size.width, size.height),
//     [size]
//   );
//   useFrame(() => composer?.current?.render(), 1);
//   return (
//     <effectComposer ref={composer} args={[gl]}>
//       <renderPass attachArray="passes" scene={scene} camera={camera} />
//       {/* <sSAOPass
//         attachArray="passes"
//         args={[scene, camera, 1024, 1024]}
//         kernelRadius={0.8}
//         maxDistance={0.4}
//       /> */}
//       <unrealBloomPass attachArray="passes" args={[undefined, 1.6, 1, 0.5]} />
//       <shaderPass
//         attachArray="passes"
//         args={[FXAAShader]}
//         material-uniforms-resolution-value={[1 / size.width, 1 / size.height]}
//       />
//       {/* <outlinePass
//         attachArray="passes"
//         args={[aspect, scene, camera]}
//         selectedObjects={hovered}
//         // visibleEdgeColor="white"
//         visibleEdgeColor="white"
//         edgeStrength={50}
//         edgeThickness={1}
//       /> */}
//     </effectComposer>
//   );
// }

/* ------------------------------ Canvas World ------------------------------ */
const CanvasWorld = memo(() => (
  <CanvasWithOrbitControls camera={{ position: [0, 0, 150], fov: 50 }}>
    {/* @ts-ignore ignoring the color warning message */}
    <color attach="background" args={["#222222"]} />
    <ambientLight />
    <pointLight position={[10, 10, 10]} />

    {/* Main background globe */}
    <Planet6 />

    {/* Country Overlay Interface */}
    <Countries />

    {/* Post-render effects */}
    {/* <Effects /> */}
  </CanvasWithOrbitControls>
));

/* ---------------------------------- Scene --------------------------------- */
export const Scene13 = () => {
  const countriesStatus = useRecoilValue(countriesStatusState);

  const selectedCountries = Object.entries(countriesStatus)
    .filter(([key, value]) => value === CountryStatus.selected)
    .map(([key, value]) => parseInt(key));

  const displayForSelectedCountries = selectedCountries.map((countryId) => {
    const name = getCountryNameFromId(countryId);
    const pngSrc = FLAG_PNGS_BY_NAME?.[name];
    let flagImage;

    if (pngSrc) {
      const imageDetails = Image.resolveAssetSource(pngSrc);
      const { height: heightTemp = 40, width: widthTemp = 40 } = imageDetails;
      const imgSizeRatio = heightTemp && widthTemp ? widthTemp / heightTemp : 1;
      const imgH = 40;
      const imgW = imgH * imgSizeRatio;
      flagImage = (
        <Image style={[{ width: imgW, height: imgH }]} source={pngSrc} />
      );
    }

    return (
      <View key={countryId}>
        <Text>{name}</Text>
        <View>{flagImage}</View>
      </View>
    );
  });

  return (
    <View style={styles.container}>
      <CanvasWorld />

      <View style={styles.infoContainer}>
        <View style={styles.infoItem}>
          <Text>Explore the globe by clicking countries</Text>
          {displayForSelectedCountries}
        </View>
      </View>
    </View>
  );
};

export default Scene13;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "black",
  },
  infoContainer: {
    position: "absolute",
    display: "flex",
    flexDirection: "row",
    bottom: 0,
    left: 0,
    height: 100,
    width: "100%",
  },
  infoItem: {
    flex: 1,
    backgroundColor: "#FF3",
    height: 100,
  },
});
