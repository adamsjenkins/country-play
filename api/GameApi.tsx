/**
 * The game hook
 */
import React, { useEffect, useRef } from "react";
import { useRecoilState, useRecoilValue } from "recoil";

import { CountryStatus } from "../types";
import { COUNTRIES_DATA_FEATURES, COUNTRY_ID_GLOBE } from "../constants";
import {
  // getCountryNameFromId,
  getCountrySubregionFromId,
} from "../libs/geoJson/geoJSONHelpers";
import { GameMode } from "../types";
import {
  sayContinentByCountryId,
  sayCountryById,
  saySubregionByCountryId,
} from "./MySpeechApi";

// Recoil Atoms
import {
  countriesStatusState,
  countryStatusState,
  clearAllSubregionsState,
  clearThenDisableAllSubregionsExceptSelectedState,
} from "../recoil/countries/atom";

import {
  gameModeState,
  gmSelectedSubregionState,
  gmSelectedContinentState,
  gmSelectedCountryState,
  gmSelectedItemState,
} from "../recoil/gameData/atom";

export type idFunction = (id: number) => void;

/**
 * It's easier for this to exist here because we'll only need to define the
 * type within this file.
 * And when we copy a new function that should be included it will exist
 * right in this file anyway.
 */
type Game = {
  test: () => void;
  clickedCountry: idFunction;
  deselectAllCountries: () => void;
  toggleGameMode: () => void;
  confirmSelection: () => void;
  //  setCountryIdToGuess: idFunction;
  //  getNewCountryToGuess: () => void;
  //  setGameMod: (gameMode: GameMode) => void;
  //  selectCountry: idFunction;
};

/**
 * This api allows us to call actions which will then have
 * the business logic define for calling and updating the
 * appropriate recoil states.
 */
export let GameApi: Game;

/**
 * This is used to ensure multiple countries are called at the same time
 * (e.g. if a country is on the other side of the globe and clicking
 * triggers it, just ignore it
 */
let lastCountryClickedTimestamp = 0;
const throttleTimeBetweenCountryClicked = 200;

/**
 * This should be added at the highest level but within the <RecoilRoot>...</RecoilRoot>,
 * because functions within GameApiRoot will be using Recoil (therefore, must
 * existing after <RecoilRoot> is defined.
 */
export const GameApiRoot: React.FC = () => {
  // Some side-effect recoil hooks
  const [, setClearThenDisableAllSubregionsExceptSelected] = useRecoilState(
    clearThenDisableAllSubregionsExceptSelectedState
  );
  const [, setClearAllSubregions] = useRecoilState(clearAllSubregionsState);

  const [countriesStatus, setCountriesStatus] =
    useRecoilState(countriesStatusState);
  const refCountriesStatus = useRef(countriesStatus);
  refCountriesStatus.current = countriesStatus;

  // Game Mode
  const [gameMode, setGameMode] = useRecoilState(gameModeState);
  const refGameMode = useRef(gameMode);
  refGameMode.current = gameMode;

  // Selected Item for specific Game Mode
  const selectedGameModeItem = useRecoilValue(gmSelectedItemState);
  const refSelectedGameModeItem = useRef(selectedGameModeItem);
  refSelectedGameModeItem.current = selectedGameModeItem;

  // Selected Subregion
  const [selectedSubregion, setSelectedSubregion] = useRecoilState(
    gmSelectedSubregionState
  );
  const refSelectedSubregion = useRef(selectedSubregion);
  refSelectedSubregion.current = selectedSubregion;

  // Selected Continent
  const [selectedContinent, setSelectedContinent] = useRecoilState(
    gmSelectedContinentState
  );
  const refSelectedContinent = useRef(selectedContinent);
  refSelectedContinent.current = selectedContinent;

  // Selected Country
  const [selectedCountry, setSelectedCountry] = useRecoilState(
    gmSelectedCountryState
  );
  const refSelectedCountry = useRef(selectedCountry);
  refSelectedCountry.current = selectedCountry;

  // =========================================================
  /** Initialize the individual countryStatus Store */
  let countryStatusByCountryId = {};
  COUNTRIES_DATA_FEATURES.forEach((Country, i) => {
    const [countryStatus, setCountryStatus] = useRecoilState(
      countryStatusState(i)
    );
    countryStatusByCountryId[i] = {
      get: countryStatus,
      set: setCountryStatus,
    };
  });

  /**
   * Assign necessary functionos to instance of the Game class
   */
  useEffect(() => {
    GameApi = {
      test: () => console.log(3),
      // test,
      clickedCountry: throttledClickCountry,
      deselectAllCountries,

      // Game Mode
      toggleGameMode,
      confirmSelection,
      //  setCountryIdToGuess,
      //  getNewCountryToGuess,
      //  setGameMod,
      //  selectCountry,
    };
  }, []);

  /**
   * The delay time between valid clicks (in ms)
   * this ensure only the first clicked country is used.
   * because otherwise if another country is behind this country
   * to would be trigged as well.
   *
   * Experimentally, quickly tapping should be result in around
   * 100ms at the fastest.
   * And extra click from extra country behind initial touch is
   * max 5ms.
   */
  const throttledClickCountry = async (countryId: number) => {
    const newTimestamp = new Date().getTime();

    if (
      newTimestamp - lastCountryClickedTimestamp >
      throttleTimeBetweenCountryClicked
    ) {
      lastCountryClickedTimestamp = newTimestamp;

      /**
       * When countryId = COUNTRY_ID_GLOBE (i.e. -1),
       * this is used to stop countries from being clicked through
       * the globe, by making the globe getting clicked calls this
       * with countryId = -1.
       * Thereby stopping any countries on other side of globe
       * from being called.
       */
      if (countryId === COUNTRY_ID_GLOBE) return;

      switch (refGameMode.current) {
        case GameMode.ExploreMap:
          handleCountryClickInSelectAllGameMode(countryId);
          break;
        case GameMode.SelectCountryInWorld:
          handleCountryClickInSelectCountryInWorld(countryId);
          break;
        case GameMode.SelectContinent:
          handleCountryClickInSelectContinent(countryId);
          break;
        case GameMode.SelectSubregion:
          handleCountryClickInSelectSubregionGameMode(countryId);
          break;
        default:
          break;
      }
    }
  };

  const toggleGameMode = () => {
    deselectAllCountries();

    // Currently we are forcing one game mode.
    setGameMode(GameMode.SelectCountryInWorld);

    // if (refGameMode.current === GameMode.ExploreMap) {
    //   console.log(2);
    //   return setGameMode(GameMode.SelectContinent);
    // }

    // if (refGameMode.current === GameMode.SelectContinent) {
    //   console.log(3);
    //   return setGameMode(GameMode.SelectSubregion);
    // }

    // if (refGameMode.current === GameMode.SelectSubregion) {
    //   console.log(4);
    //   return setGameMode(GameMode.ExploreMap);
    // }
  };

  const selectCountry = (countryId: number) => {
    const currentCountriesStatus = refCountriesStatus.current;

    countryStatusByCountryId[countryId].set(CountryStatus.selected);

    return setCountriesStatus({
      ...currentCountriesStatus,
      [countryId]: CountryStatus.selected,
    });
  };

  const deselectCountry = (countryId: number) => {
    const currentCountriesStatus = refCountriesStatus.current;

    countryStatusByCountryId[countryId].set(undefined);

    const tempCountriesStatus = { ...currentCountriesStatus };
    delete tempCountriesStatus[countryId];

    return setCountriesStatus(tempCountriesStatus);
  };

  const deselectAllCountries = () => {
    const currentCountriesStatus = refCountriesStatus.current;
    const selectedIds = Object.keys(currentCountriesStatus);
    selectedIds.forEach((countryId) => {
      // if (countryStatusByCountryId[countryId]
      countryStatusByCountryId[countryId].set(undefined);
    });

    /**
     * Note set values for countriesStatus and countryStatus[countryId] separately
     * because for some reason looking through and calling GameApi.deselectCountry
     * doesn't work since the currentCountriesStatus value keeps being its original
     * value.
     */
    setCountriesStatus({});
  };

  // --------------------------------------------
  // Game Mode Specific functionality
  const handleCountryClickInSelectCountryInWorld = async (
    countryId: number
  ) => {
    const currentCountriesStatus = refCountriesStatus.current;
    if (!currentCountriesStatus) {
      return;
    }

    deselectAllCountries();

    sayCountryById(countryId);

    setTimeout(() => {
      if (currentCountriesStatus[countryId] === CountryStatus.selected) {
        return deselectCountry(countryId);
      }

      if (currentCountriesStatus[countryId] === undefined) {
        return selectCountry(countryId);
      }
    }, 0);
  };

  const handleCountryClickInSelectContinent = async (countryId: number) => {
    const currentCountriesStatus = refCountriesStatus.current;
    if (!currentCountriesStatus) {
      return;
    }

    deselectAllCountries();

    sayContinentByCountryId(countryId);

    setTimeout(() => {
      if (currentCountriesStatus[countryId] === CountryStatus.selected) {
        return deselectCountry(countryId);
      }

      if (currentCountriesStatus[countryId] === undefined) {
        return selectCountry(countryId);
      }
    }, 0);
  };

  const handleCountryClickInSelectAllGameMode = async (countryId: number) => {
    const currentCountriesStatus = refCountriesStatus.current;
    if (!currentCountriesStatus) {
      return;
    }

    sayCountryById(countryId);

    console.log("NOTE THE COUNTRY SELECT PART DONT WORK YET, IT PAUSES");
  };

  const handleCountryClickInSelectSubregionGameMode = async (
    countryId: number
  ) => {
    const currentCountriesStatus = refCountriesStatus.current;
    if (!currentCountriesStatus) {
      return;
    }

    saySubregionByCountryId(countryId);

    const subregion = getCountrySubregionFromId(countryId);
    setSelectedSubregion(subregion);

    /**
     * Using setTimeout to ensure the time of both deselect and select
     * works as it should
     */
    // setTimeout(() => {
    //   if (currentCountriesStatus[countryId] === CountryStatus.selected) {
    //     return deselectCountry(countryId);
    //   }

    //   if (currentCountriesStatus[countryId] === undefined) {
    //     return selectCountry(countryId);
    //   }
    // }, 0);
  };

  const _initGameModeSelectSubregion = () => {
    setClearAllSubregions(undefined);
    setGameMode(GameMode.SelectSubregion);
  };

  const _initGameModeSelectCountryInSubregion = () => {
    setClearThenDisableAllSubregionsExceptSelected(undefined);
    setGameMode(GameMode.SelectCountryInSubregion);
  };

  const confirmSelection = () => {
    switch (gameMode) {
      case GameMode.SelectSubregion:
        if (refSelectedGameModeItem.current) {
          // Clean up any files that aren't used anymore.
          setSelectedSubregion(undefined);
          _initGameModeSelectCountryInSubregion();
        }
        break;
      case GameMode.SelectCountryInSubregion:
      default:
        _initGameModeSelectSubregion();
        break;
    }

    console.log("confirmSelection!");
    // Depending on the game mode, specify the confirmation.
    if (refSelectedGameModeItem.current) {
      // deselectAllCountries();
      setSelectedSubregion(undefined);
      setGameMode(GameMode.SelectCountryInSubregion);
    } else {
      setGameMode(GameMode.SelectSubregion);
    }
  };

  /** We don't need to return anything because this is just acting as a hook really */
  return null;
};

// NOTE: Do not use export default, because for whatever reason
// importing as `import GameApi from "..../GameApi"` causes issues,
// while `import { GameApi } from "..../GameApi"` works great.
//
// export default GameApi;
