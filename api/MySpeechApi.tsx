import * as Speech from "expo-speech";

import {
  getCountryContinentFromId,
  getCountryNameFromId,
  getCountrySubregionFromId,
} from "../libs/geoJson/geoJSONHelpers";

/** This calls speech, and also stop any previously playing speech */
export const say = (msg: string) => {
  Speech.stop();
  Speech.speak(msg, {
    onStart: () => {},
    onDone: () => {},
  });
};

/** Say country name from country id*/
export const sayCountryById = (countryId) => {
  const countryName = getCountryNameFromId(countryId);
  if (countryName.length >= 1) {
    say(countryName)
  }
};

/** Say subregion from country id */
export const saySubregionByCountryId = (countryId) => {
  const subregion = getCountrySubregionFromId(countryId);
  if (subregion.length >= 1) {
    say(subregion);
  }
};

/** Say continent from country id */
export const sayContinentByCountryId = (countryId) => {
  const continent = getCountryContinentFromId(countryId);
  if (continent.length >= 1) {
    say(continent);
  }
};
